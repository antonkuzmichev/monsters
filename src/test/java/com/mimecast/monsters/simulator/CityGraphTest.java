package com.mimecast.monsters.simulator;

import com.mimecast.monsters.domain.City;
import com.mimecast.monsters.domain.Direction;
import com.mimecast.monsters.domain.Monster;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("Duplicates")
public class CityGraphTest {

    @Test(expected = IllegalStateException.class)
    public void addMonsters_NoCities_ExceptionThrown() {
        new CityGraph().addMonsters(1);
    }

    @Test(expected = IllegalStateException.class)
    public void addMonsters_CitiesNumberLessThanMonsters_ExceptionThrown() {
        CityGraph graph = new CityGraph();
        graph.setCities(Collections.singletonList(new City("Denalmo")));
        graph.addMonsters(2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addMonsters_Zero_ExceptionThrown() {
        new CityGraph().addMonsters(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addMonsters_NegativeNumber_ExceptionThrown() {
        new CityGraph().addMonsters(-1);
    }

    @Test
    public void addMonsters_OneMonster_Added() {
        City city = new City("Denalmo");
        CityGraph graph = new CityGraph();
        graph.setCities(Collections.singletonList(city));
        graph.addMonsters(1);

        assertEquals("Incorrect number of monsters", 1, city.getMonsters().size());
    }

    @Test
    public void unleashMonsters_OneMonsterNotConnectedCities_Stayed() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
        }});
        city1.addMonster(new Monster(1));
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of monsters for city1", 1, city1.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city1", 0, city1.getNeighbours().size());
        assertEquals("Incorrect number of monsters for city2", 0, city2.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city2", 0, city2.getNeighbours().size());
    }

    @Test
    public void unleashMonsters_OneMonsterConnectedCities_Moved() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        city1.setNeighbour(Direction.SOUTH, city2);
        city2.setNeighbour(Direction.NORTH, city1);
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
        }});
        city1.addMonster(new Monster(1));
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of monsters for city1", 0, city1.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city1", 1, city1.getNeighbours().size());
        assertTrue("Incorrect neighbour for city1", city2 == city1.getNeighbours().get(Direction.SOUTH));
        assertEquals("Incorrect number of monsters for city2", 1, city2.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city2", 1, city2.getNeighbours().size());
        assertTrue("Incorrect neighbour for city2", city1 == city2.getNeighbours().get(Direction.NORTH));
    }

    @Test
    public void unleashMonsters_TwoMonstersNotConnectedCities_Stayed() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
        }});
        Monster monster1 = new Monster(1);
        city1.addMonster(monster1);
        Monster monster2 = new Monster(2);
        city2.addMonster(monster2);
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of monsters for city1", 1, city1.getMonsters().size());
        assertTrue("Incorrect monster for city1", monster1 == city1.getMonsters().iterator().next());
        assertEquals("Incorrect number of neighbours for city1", 0, city1.getNeighbours().size());
        assertEquals("Incorrect number of monsters for city2", 1, city2.getMonsters().size());
        assertTrue("Incorrect monster for city2", monster2 == city2.getMonsters().iterator().next());
        assertEquals("Incorrect number of neighbours for city2", 0, city2.getNeighbours().size());
    }

    @Test
    public void unleashMonsters_TwoMonstersConnectedCities_Moved() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        city1.setNeighbour(Direction.SOUTH, city2);
        city2.setNeighbour(Direction.NORTH, city1);
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
        }});
        Monster monster1 = new Monster(1);
        city1.addMonster(monster1);
        Monster monster2 = new Monster(2);
        city2.addMonster(monster2);
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of monsters for city1", 1, city1.getMonsters().size());
        System.out.println("" + city1.getMonsters().iterator().next());
        assertTrue("Incorrect monster for city1", monster2 == city1.getMonsters().iterator().next());
        assertEquals("Incorrect number of neighbours for city1", 1, city1.getNeighbours().size());
        assertTrue("Incorrect neighbour for city1", city2 == city1.getNeighbours().get(Direction.SOUTH));
        assertEquals("Incorrect number of monsters for city2", 1, city2.getMonsters().size());
        assertTrue("Incorrect monster for city2", monster1 == city2.getMonsters().iterator().next());
        assertEquals("Incorrect number of neighbours for city2", 1, city2.getNeighbours().size());
        assertTrue("Incorrect neighbour for city2", city1 == city2.getNeighbours().get(Direction.NORTH));
    }

    @Test
    public void unleashMonsters_TwoMonstersThreeConnectedCities_Unleashed() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        City city3 = new City("Mulule");
        city1.setNeighbour(Direction.SOUTH, city2);
        city2.setNeighbour(Direction.NORTH, city1);
        city2.setNeighbour(Direction.SOUTH, city3);
        city3.setNeighbour(Direction.NORTH, city2);
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
            add(city3);
        }});
        Monster monster1 = new Monster(1);
        city1.addMonster(monster1);
        Monster monster2 = new Monster(2);
        city3.addMonster(monster2);
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of monsters for city1", 0, city1.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city1", 0, city1.getNeighbours().size());
        assertEquals("Incorrect number of monsters for city2", 2, city2.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city2", 0, city2.getNeighbours().size());
        assertEquals("Incorrect number of monsters for city3", 0, city3.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city3", 0, city3.getNeighbours().size());
    }

    @Test
    public void unleashMonsters_ThreeMonstersFourConnectedCities_Unleashed() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        City city3 = new City("Mulule");
        City city4 = new City("Mesmega");
        city1.setNeighbour(Direction.SOUTH, city2);
        city2.setNeighbour(Direction.NORTH, city1);
        city2.setNeighbour(Direction.SOUTH, city3);
        city3.setNeighbour(Direction.NORTH, city2);
        city2.setNeighbour(Direction.EAST, city4);
        city4.setNeighbour(Direction.WEST, city2);
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
            add(city3);
            add(city4);
        }});
        Monster monster1 = new Monster(1);
        city1.addMonster(monster1);
        Monster monster2 = new Monster(2);
        city3.addMonster(monster2);
        Monster monster3 = new Monster(3);
        city4.addMonster(monster3);
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of monsters for city1", 0, city1.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city1", 0, city1.getNeighbours().size());
        assertEquals("Incorrect number of monsters for city2", 3, city2.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city2", 0, city2.getNeighbours().size());
        assertEquals("Incorrect number of monsters for city3", 0, city3.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city3", 0, city3.getNeighbours().size());
        assertEquals("Incorrect number of monsters for city4", 0, city4.getMonsters().size());
        assertEquals("Incorrect number of neighbours for city4", 0, city4.getNeighbours().size());
    }

    @Test
    public void getCities_OneCity_Got() {
        City city = new City("Denalmo");
        CityGraph graph = new CityGraph();
        graph.setCities(Collections.singletonList(city));
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of cities", 1, graph.getCities().size());
        assertTrue("City not found in result list", graph.getCities().contains(city));
    }

    @Test
    public void getCities_NoMonstersTwoDisconnectedCities_Got() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
        }});
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of cities", 2, graph.getCities().size());
        assertTrue("City1 not found in result list", graph.getCities().contains(city1));
        assertTrue("City2 not found in result list", graph.getCities().contains(city2));
    }

    @Test
    public void getCities_OneMonsterTwoConnectedCities_Got() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        city1.setNeighbour(Direction.SOUTH, city2);
        city2.setNeighbour(Direction.NORTH, city1);
        city1.addMonster(new Monster(1));
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
        }});
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of cities", 2, graph.getCities().size());
        assertTrue("City1 not found in result list", graph.getCities().contains(city1));
        assertTrue("City2 not found in result list", graph.getCities().contains(city2));
    }

    @Test
    public void getCities_TwoMonstersThreeConnectedCities_Got() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        City city3 = new City("Mulule");
        city1.setNeighbour(Direction.SOUTH, city2);
        city2.setNeighbour(Direction.NORTH, city1);
        city2.setNeighbour(Direction.SOUTH, city3);
        city3.setNeighbour(Direction.NORTH, city2);
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
            add(city3);
        }});
        Monster monster1 = new Monster(1);
        city1.addMonster(monster1);
        Monster monster2 = new Monster(2);
        city3.addMonster(monster2);
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of cities", 2, graph.getCities().size());
        assertTrue("City1 not found in result list", graph.getCities().contains(city1));
        assertTrue("City3 not found in result list", graph.getCities().contains(city3));
    }

    @Test
    public void getCities_ThreeMonstersFourConnectedCities_Got() {
        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        City city3 = new City("Mulule");
        City city4 = new City("Mesmega");
        city1.setNeighbour(Direction.SOUTH, city2);
        city2.setNeighbour(Direction.NORTH, city1);
        city2.setNeighbour(Direction.SOUTH, city3);
        city3.setNeighbour(Direction.NORTH, city2);
        city2.setNeighbour(Direction.EAST, city4);
        city4.setNeighbour(Direction.WEST, city2);
        CityGraph graph = new CityGraph();
        graph.setCities(new ArrayList<City>() {{
            add(city1);
            add(city2);
            add(city3);
            add(city4);
        }});
        Monster monster1 = new Monster(1);
        city1.addMonster(monster1);
        Monster monster2 = new Monster(2);
        city3.addMonster(monster2);
        Monster monster3 = new Monster(3);
        city4.addMonster(monster3);
        graph.unleashMonsters(1);

        assertEquals("Incorrect number of cities", 3, graph.getCities().size());
        assertTrue("City1 not found in result list", graph.getCities().contains(city1));
        assertTrue("City3 not found in result list", graph.getCities().contains(city3));
        assertTrue("City4 not found in result list", graph.getCities().contains(city4));
    }
}
