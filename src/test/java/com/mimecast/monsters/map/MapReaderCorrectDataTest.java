package com.mimecast.monsters.map;

import com.mimecast.monsters.domain.Direction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class MapReaderCorrectDataTest {

    @SuppressWarnings("Duplicates")
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {
                    "Denalmo",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("Denalmo", new HashMap<>());
                    }}
                }, {
                    "!*W:X\\&Gi/w∼>g/h#WQ@",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("!*W:X\\&Gi/w∼>g/h#WQ@", new HashMap<>());
                    }}
                }, {
                    "Denalmo north=Agixo-A",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("Denalmo", new HashMap<Direction, String>() {{
                            put(Direction.NORTH, "Agixo-A");
                        }});
                    }}
                }, {
                    "Denalmo south=Agixo-A",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("Denalmo", new HashMap<Direction, String>() {{
                            put(Direction.SOUTH, "Agixo-A");
                        }});
                    }}
                }, {
                    "Denalmo east=Agixo-A",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("Denalmo", new HashMap<Direction, String>() {{
                            put(Direction.EAST, "Agixo-A");
                        }});
                    }}
                }, {
                    "Denalmo west=Agixo-A",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("Denalmo", new HashMap<Direction, String>() {{
                            put(Direction.WEST, "Agixo-A");
                        }});
                    }}
                }, {
                    "Denalmo north=Agixo-A south=Amolusnisnu east=Elolesme west=Migina",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("Denalmo", new HashMap<Direction, String>() {{
                            put(Direction.NORTH, "Agixo-A");
                            put(Direction.SOUTH, "Amolusnisnu");
                            put(Direction.EAST, "Elolesme");
                            put(Direction.WEST, "Migina");
                        }});
                    }}
                }, {
                        "Denalmo south=Amolusnisnu west=Migina east=Elolesme north=Agixo-A",
                        new HashMap<String, Map<Direction, String>>() {{
                            put("Denalmo", new HashMap<Direction, String>() {{
                                put(Direction.NORTH, "Agixo-A");
                                put(Direction.SOUTH, "Amolusnisnu");
                                put(Direction.EAST, "Elolesme");
                                put(Direction.WEST, "Migina");
                            }});
                        }}
                }, {
                    "Denalmo north=Agixo-A\n" +
                            "Agixo-A south=Denalmo",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("Denalmo", new HashMap<Direction, String>() {{
                                put(Direction.NORTH, "Agixo-A");
                            }});
                        put("Agixo-A", new HashMap<Direction, String>() {{
                                put(Direction.SOUTH, "Denalmo");
                            }});
                    }}
                }, {
                    "Denalmo east=Agixo-A\n" +
                            "Agixo-A west=Denalmo",
                    new HashMap<String, Map<Direction, String>>() {{
                        put("Denalmo", new HashMap<Direction, String>() {{
                            put(Direction.EAST, "Agixo-A");
                        }});
                        put("Agixo-A", new HashMap<Direction, String>() {{
                            put(Direction.WEST, "Denalmo");
                        }});
                    }}
                }
        });
    }

    private final String input;
    private final Map<String, Map<Direction, String>> expected;

    public MapReaderCorrectDataTest(String input, Map<String, Map<Direction, String>> expected) {
        this.input = input;
        this.expected= expected;
    }

    @Test
    public void read_DataCorrect_Read() throws UnsupportedEncodingException {
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(Charset.forName("UTF-8")));
        assertEquals(expected, new MapReader().read(inputStream));
    }
}
