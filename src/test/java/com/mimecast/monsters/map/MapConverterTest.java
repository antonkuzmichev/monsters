package com.mimecast.monsters.map;

import com.mimecast.monsters.domain.City;
import com.mimecast.monsters.domain.Direction;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class MapConverterTest {

    @Test
    public void getCities_NoNeighbours_Got() {
        Map<String, Map<Direction, String>> map = new HashMap<String, Map<Direction, String>>() {{
            put("Denalmo", new HashMap<>());
        }};
        List<City> result = new MapConverter().getCities(map);
        City expected = new City("Denalmo");
        assertNotNull("City list must not be null", result);
        assertEquals("City list size is incorrect", 1, result.size());
        assertTrue("City not found in result list", result.contains(expected));
        assertTrue("City neighbours size is incorrect", result.get(0).getNeighbours().isEmpty());
    }

    @Test
    public void getCities_OneNeighbour_Got() {
        Map<String, Map<Direction, String>> map = new HashMap<String, Map<Direction, String>>() {{
            put("Denalmo", new HashMap<Direction, String>() {{
                put(Direction.NORTH, "Agixo-A");
            }});
            put("Agixo-A", new HashMap<Direction, String>() {{
                put(Direction.SOUTH, "Denalmo");
            }});
        }};

        List<City> result = new MapConverter().getCities(map);
        assertNotNull("City list must not be null", result);
        assertEquals("City list size is incorrect", 2, result.size());

        City city1 = new City("Denalmo");
        City city2 = new City("Agixo-A");
        assertTrue("City 1 not found in result list", result.contains(city1));
        assertTrue("City 2 not found in result list", result.contains(city2));
        city1 = getObject(result, city1);
        city2 = getObject(result, city2);
        assertEquals("City 1 neighbours size is incorrect", 1, city1.getNeighbours().size());
        assertEquals("City 2 neighbours size is incorrect", 1, city2.getNeighbours().size());
        assertTrue("City 1 neighbours are incorrect", city1.getNeighbours().get(Direction.NORTH) == city2);
        assertTrue("City 2 neighbours are incorrect", city2.getNeighbours().get(Direction.SOUTH) == city1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCities_UnknownNeighbour_ExceptionThrown() {
        Map<String, Map<Direction, String>> map = new HashMap<String, Map<Direction, String>>() {{
            put("Denalmo", new HashMap<Direction, String>() {{
                put(Direction.SOUTH, "Agixo-A");
            }});
        }};
        new MapConverter().getCities(map);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCities_OneWayRoad_ExceptionThrown() {
        Map<String, Map<Direction, String>> map = new HashMap<String, Map<Direction, String>>() {{
            put("Denalmo", new HashMap<Direction, String>() {{
                put(Direction.SOUTH, "Agixo-A");
            }});
            put("Agixo-A", new HashMap<>());
        }};
        new MapConverter().getCities(map);
    }

    private <T> T getObject(List<T> list, T object) {
        int index = list.indexOf(object);
        return list.get(index);
    }
}
