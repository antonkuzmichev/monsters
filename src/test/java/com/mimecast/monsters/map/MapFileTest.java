package com.mimecast.monsters.map;


import org.junit.Test;

public class MapFileTest {

    @Test
    public void load_FileExists_Loaded() {
        new MapFile(new MapReader()).load("src/test/resources/map.txt");
    }

    @Test(expected = IllegalArgumentException.class)
    public void load_FileNotExists_ExceptionThrown() {
        new MapFile(new MapReader()).load("src/test/resources/map1.txt");
    }
}