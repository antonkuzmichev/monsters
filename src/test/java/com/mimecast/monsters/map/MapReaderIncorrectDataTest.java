package com.mimecast.monsters.map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class MapReaderIncorrectDataTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {
                        "Dena=lmo",
                        IllegalArgumentException.class
                }, {
                        "Denalmo  north=Agixo-A",
                        IllegalArgumentException.class
                }, {
                        "Denalmo north=Agixo-A  south=Amolusnisnu east=Elolesme west=Migina",
                        IllegalArgumentException.class
                }, {
                        "Denalmo North=Agixo-A",
                        IllegalArgumentException.class
                }, {
                        "Denalmo north1=Agixo-A",
                        IllegalArgumentException.class
                }, {
                        "Denalmo north=Agixo-A\n" +
                            "Denalmo south=Amolusnisnu",
                        IllegalArgumentException.class
                }
        });
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private final String input;
    private final Class expectedException;

    public MapReaderIncorrectDataTest(String input, Class expectedException) {
        this.input = input;
        this.expectedException= expectedException;
    }

    @Test
    public void read_DataIncorrect_ExceptionThrown() throws UnsupportedEncodingException {
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(Charset.forName("UTF-8")));
        exception.expect(expectedException);
        new MapReader().read(inputStream);
    }
}
