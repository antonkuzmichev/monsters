package com.mimecast.monsters.concurrent;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ActionLock extends ReentrantReadWriteLock {

    private static final AtomicInteger order = new AtomicInteger(0);
    private final int id = order.incrementAndGet();

    public int getId() { return id; }
}
