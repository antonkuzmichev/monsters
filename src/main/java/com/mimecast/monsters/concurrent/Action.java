package com.mimecast.monsters.concurrent;

import java.util.function.Consumer;

public interface Action<T> {

    void perform(Consumer<T> operation, T object);
}
