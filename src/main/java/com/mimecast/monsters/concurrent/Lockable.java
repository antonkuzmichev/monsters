package com.mimecast.monsters.concurrent;

public interface Lockable {

    ActionLock getLock();
}
