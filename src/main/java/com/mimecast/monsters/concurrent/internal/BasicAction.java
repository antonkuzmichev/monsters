package com.mimecast.monsters.concurrent.internal;

import com.mimecast.monsters.concurrent.Action;

import java.util.concurrent.locks.Lock;
import java.util.function.Consumer;

public class BasicAction<T> implements Action<T> {

    private final Lock lock;

    public BasicAction(Lock lock) {
        this.lock = lock;
    }

    @Override
    public void perform(Consumer<T> operation, T object) {
        lock.lock();
        try {
            operation.accept(object);
        } finally {
            lock.unlock();
        }
    }
}
