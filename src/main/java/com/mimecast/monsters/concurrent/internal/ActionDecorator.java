package com.mimecast.monsters.concurrent.internal;

import com.mimecast.monsters.concurrent.Action;

import java.util.concurrent.locks.Lock;
import java.util.function.Consumer;

public class ActionDecorator<T> implements Action<T> {

    private final Lock lock;
    private final Action<T> action;

    public ActionDecorator(Lock lock, Action<T> action) {
        this.lock = lock;
        this.action = action;
    }

    @Override
    public void perform(Consumer<T> operation, T object) {
        lock.lock();
        try {
            action.perform(operation, object);
        } finally {
            lock.unlock();
        }
    }
}
