package com.mimecast.monsters.concurrent;

import com.mimecast.monsters.concurrent.internal.ActionDecorator;
import com.mimecast.monsters.concurrent.internal.BasicAction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ActionFactory {

    public static <T extends Lockable> Action<T> createUpdateAction(List<T> objects) {
        if (objects.isEmpty()) {
            throw new IllegalArgumentException("Empty list of objects");
        }

        objects = new ArrayList<>(objects);
        Collections.sort(objects, Comparator.comparingInt(o -> o.getLock().getId()));
        Action<T> result = new BasicAction<>(objects.get(0).getLock().writeLock());

        for (int i = 1; i < objects.size(); i++) {
            result = new ActionDecorator<>(objects.get(i).getLock().writeLock(), result);
        }

        return result;
    }

    private ActionFactory() {}
}
