package com.mimecast.monsters.domain;

import java.util.HashMap;
import java.util.Map;

public enum Direction {
    NORTH("north", "south"),
    SOUTH("south", "north"),
    EAST("east", "west"),
    WEST("west", "east");

    private final String value;
    private final String opposite;

    Direction(String value, String opposite) {
        this.value = value;
        this.opposite = opposite;
    }

    public Direction opposite() { return fromString(opposite); }

    @Override
    public String toString() { return value; }

    private static final Map<String, Direction> stringToEnum = new HashMap<>();

    static {
        for (Direction d : values()) {
            stringToEnum.put(d.toString(), d);
        }
    }

    public static Direction fromString(String status) { return stringToEnum.get(status); }
}
