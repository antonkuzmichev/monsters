package com.mimecast.monsters.domain;

import com.mimecast.monsters.concurrent.ActionLock;
import com.mimecast.monsters.concurrent.Lockable;

import java.util.*;

public class City implements Lockable {

    private static final Random random = new Random();

    private final String name;
    private final Map<Direction, City> neighbours = new HashMap<>();
    private final Set<Monster> monsters = new HashSet<>();
    private final ActionLock lock = new ActionLock();

    public City(String name) {
        if (name == null) {
            throw new NullPointerException("Name cannot be null");
        }
        this.name = name;
    }

    public String getName() { return name; }

    @Override
    public ActionLock getLock() { return lock; }

    public Map<Direction, City> getNeighbours() {
        return Collections.unmodifiableMap(neighbours);
    }

    public void setNeighbour(Direction direction, City city) {
        neighbours.put(direction, city);
    }

    public void removeNeighbour(Direction direction) {
        neighbours.remove(direction);
    }

    public City randomNeighbour() {
        List<City> neighboursList = new ArrayList<>(neighbours.values());
        return neighboursList.isEmpty() ? null : neighboursList.get(random.nextInt(neighboursList.size()));
    }

    public void clearNeighbours() {
        neighbours.clear();
    }

    public void addMonster(Monster monster) {
        monsters.add(monster);
    }

    public void removeMonster(Monster monster) {
        monsters.remove(monster);
    }

    public Set<Monster> getMonsters() {
        return Collections.unmodifiableSet(monsters);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof City)) {
            return false;
        }
        City city = (City) o;
        return name.equals(city.name);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return name;
    }
}