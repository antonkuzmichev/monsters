package com.mimecast.monsters;


import com.mimecast.monsters.map.MapConverter;
import com.mimecast.monsters.map.MapFile;
import com.mimecast.monsters.map.MapPrinter;
import com.mimecast.monsters.map.MapReader;
import com.mimecast.monsters.simulator.EvilWorld;
import com.mimecast.monsters.simulator.CityGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    private static final Logger log = LoggerFactory.getLogger(App.class);
    private static final int MAX_MOVES = 100000;

    public static void main(String... args) throws Exception {

        try {
            if (args.length != 2) {
                throw new IllegalArgumentException("Invalid number of arguments. Program must have 2 arguments: " +
                        "path to file with map of cities and number of monsters to create");
            }

            String fileName = args[0];
            int numberOfMonsters = readNumberOfMonsters(args[1]);

            EvilWorld world = new EvilWorld(new CityGraph(), new MapConverter());
            MapFile mapFile = new MapFile(new MapReader());
            world.setMap(mapFile.load(fileName));
            world.addMonsters(numberOfMonsters);
            world.unleashMonsters(MAX_MOVES);
            new MapPrinter().print(world.getMap());

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private static int readNumberOfMonsters(String numberOfMonsters) {
        try {
            return Integer.valueOf(numberOfMonsters);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid number of monsters: " + numberOfMonsters +
                    ", must be an integer");
        }
    }
}

