package com.mimecast.monsters.simulator;

import com.mimecast.monsters.domain.Direction;
import com.mimecast.monsters.map.MapConverter;

import java.util.Map;

public class EvilWorld {

    private final CityGraph cityGraph;
    private final MapConverter mapConverter;

    public EvilWorld(CityGraph cityGraph, MapConverter mapConverter) {
        this.cityGraph = cityGraph;
        this.mapConverter = mapConverter;
    }

    public Map<String, Map<Direction, String>> getMap() {
        return mapConverter.getMap(cityGraph.getCities());
    }

    public void setMap(Map<String, Map<Direction, String>> map) {
        this.cityGraph.setCities(mapConverter.getCities(map));
    }

    public void addMonsters(int number) {
        this.cityGraph.addMonsters(number);
    }

    public void unleashMonsters(int maxMoves) {
        this.cityGraph.unleashMonsters(maxMoves);
    }
}
