package com.mimecast.monsters.simulator;

import com.mimecast.monsters.concurrent.Action;
import com.mimecast.monsters.concurrent.ActionFactory;
import com.mimecast.monsters.domain.City;
import com.mimecast.monsters.domain.Monster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CityGraph {

    private static final Logger log = LoggerFactory.getLogger(EvilWorld.class);

    private List<City> cities = new ArrayList<>();

    public void setCities(List<City> cities) {
        this.cities = new ArrayList<>(cities);
    }

    public List<City> getCities() {
        return Collections.unmodifiableList(cities);
    }

    public void addMonsters(int number) {
        if (number <= 0) {
            throw new IllegalArgumentException("Number of monsters must be greater than 0");
        }

        if (cities.isEmpty() || cities.size() < number) {
            throw new IllegalStateException("Not enough cities for so many monsters");
        }

        List<City> citiesToAddMonsters = new ArrayList<>(cities);
        if (number < cities.size()) {
            Collections.shuffle(citiesToAddMonsters);
            citiesToAddMonsters = citiesToAddMonsters.subList(0, number);
        }

        addMonsters(citiesToAddMonsters);
    }

    public void unleashMonsters(int maxMoves) {

        int i = 1;
        log.info("cities size={}", cities.size());
        do {
            log.trace("iteration {}", i++);

            Map<City, Monster> cityWithMonster = cities.parallelStream().filter(c -> c.getMonsters().size() == 1)
                    .collect(Collectors.toMap(c -> c, c -> c.getMonsters().iterator().next()));

            log.trace("cities with monsters: {}", cityWithMonster.size());

            cityWithMonster.keySet().parallelStream().forEach(c -> moveMonster(cityWithMonster.get(c), c));

            cities = cities.parallelStream().filter(c -> {
                if (c.getMonsters().size() > 1) {
                    clearNeighbours(c);
                    if (log.isInfoEnabled()) {
                        log.info(destroyMessage(c));
                    }
                    return false;
                }
                return true;
            }).collect(Collectors.toList());
            log.trace("available cities: {}", cities.size());

        } while (!cities.isEmpty() && i <= maxMoves);
    }

    private String destroyMessage(City city) {
        return String.format("%s has been destroyed by %s!", city,
                String.join(", ", city.getMonsters().stream().map(Monster::toString).collect(Collectors.toList())))
                .replaceFirst(", ([^,]+)$", " and $1");
    }

    private void moveMonster(Monster monster, City city) {
        log.trace("monster {}", monster);
        createUpdateAction(city).perform(from -> {
            log.trace("from {}", from);
            log.trace("neighbours size = {}", from.getNeighbours().size());
            if (!from.getNeighbours().isEmpty()) {
                City to = from.randomNeighbour();
                log.trace("to {}", to);
                from.removeMonster(monster);
                to.addMonster(monster);
            }
        }, city);
    }

    private void clearNeighbours(City city) {
        createUpdateAction(city).perform(from -> {
            from.getNeighbours().forEach((direction, neighbour) -> neighbour.removeNeighbour(direction.opposite()));
            from.clearNeighbours();
        }, city);
    }

    private Action<City> createUpdateAction(City city) {
        List<City> lockObjects = new LinkedList<>();
        lockObjects.add(city);
        lockObjects.addAll(city.getNeighbours().values());
        return ActionFactory.createUpdateAction(lockObjects);
    }

    private void addMonsters(List<City> cities) {
        IntStream.range(0, cities.size()).forEach(i -> {
            Monster monster = new Monster(i);
            City city = cities.get(i);
            city.addMonster(monster);
        });
    }
}
