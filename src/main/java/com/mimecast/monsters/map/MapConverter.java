package com.mimecast.monsters.map;

import com.mimecast.monsters.domain.City;
import com.mimecast.monsters.domain.Direction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapConverter {

    public List<City> getCities(Map<String, Map<Direction, String>> map) {
        validateMap(map);

        Map<String, City> nameToCity = new HashMap<>();
        map.keySet().forEach(name -> nameToCity.put(name, new City(name)));

        map.forEach((name, directionToNeighbour) -> {
            City city = nameToCity.get(name);
            directionToNeighbour.forEach((direction, neighbour) ->
                    city.setNeighbour(direction, nameToCity.get(neighbour)));
        });

        return new ArrayList<>(nameToCity.values());
    }

    public Map<String, Map<Direction, String>> getMap(List<City> cities) {
        Map<String, Map<Direction, String>> result = new HashMap<>();
        cities.forEach(c ->
                result.put(c.getName(),
                        c.getNeighbours().entrySet().stream()
                                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getName()))));
        validateMap(result);
        return result;
    }

    private static void validateMap(Map<String, Map<Direction, String>> map) {
        map.forEach((name, directionToNeighbour) ->
                directionToNeighbour.forEach((direction, neighbour) -> {
                    if (!map.containsKey(neighbour)) {
                        throw new IllegalArgumentException("Road to unknown city: " + neighbour);
                    }
                    if (!name.equals(map.get(neighbour).get(direction.opposite()))) {
                        throw new IllegalArgumentException("One way road between " + name + " and " + neighbour);
                    }
                }));
    }
}
