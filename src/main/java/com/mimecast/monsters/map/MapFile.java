package com.mimecast.monsters.map;

import com.mimecast.monsters.domain.Direction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

public class MapFile {

    private final MapReader reader;

    public MapFile(MapReader reader) {
        this.reader = reader;
    }

    public Map<String, Map<Direction, String>> load(String fileName) {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(new File(fileName));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Failed to find file " + fileName, e);
        }
        return reader.read(inputStream);
    }
}
