package com.mimecast.monsters.map;

import com.mimecast.monsters.domain.Direction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class MapPrinter {
    private static final Logger log = LoggerFactory.getLogger(MapPrinter.class);

    public void print(Map<String, Map<Direction, String>> map) {
        log.info("Remaining cities size={}", map.size());
        map.forEach((name, directionToNeighbours) -> {
            StringBuilder sb = new StringBuilder(name);

            directionToNeighbours.forEach((direction, neighbour) ->
                    sb.append(String.format(" %s=%s", direction, neighbour)));

            if (log.isInfoEnabled()) {
                log.info(sb.toString());
            }
        });
    }
}
