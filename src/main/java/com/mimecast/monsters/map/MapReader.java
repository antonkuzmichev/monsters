package com.mimecast.monsters.map;

import com.mimecast.monsters.domain.Direction;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapReader {

    public Map<String, Map<Direction, String>> read(InputStream source) {
        Map<String, Map<Direction, String>> result = new HashMap<>();

        try (Scanner scanner = new Scanner(source, "UTF-8")) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                parseCityLine(result, line);
            }
        }

        return result;
    }

    private void parseCityLine(Map<String, Map<Direction, String>> nameToNeighbours, String line) {
        String[] parts = line.split(" ");
        if (parts.length == 0 || parts.length > 5) {
            throw new IllegalArgumentException("Invalid line: " + line);
        } else {
            String cityName = parts[0];
            if (cityName.contains("=")) {
                throw new IllegalArgumentException("Invalid city name: " + cityName);
            }
            if (nameToNeighbours.containsKey(cityName)) {
                throw new IllegalArgumentException("Duplicated city name: " + cityName);
            }

            Map<Direction, String> neighbours = new HashMap<>();
            for (int i = 1; i < parts.length; i++) {
                String[] directionParts = parts[i].split("=");
                Direction direction = Direction.fromString(directionParts[0]);
                if (directionParts.length != 2 || direction == null) {
                    throw new IllegalArgumentException("Invalid direction: " + parts[i]);
                }
                neighbours.put(direction, directionParts[1]);
            }

            nameToNeighbours.put(cityName, neighbours);
        }
    }
}
