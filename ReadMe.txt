=========================
Monsters Project
=========================

To build this project use

    mvn install

To run this project from within Maven use

    mvn exec:java -Dlog4j.configuration=file:{1} "-Dexec.args={2} {3}"
	
	where 	{1} - path to log4j.properties (e.g., src/main/resources/log4j.properties)
			(2) - path to file with map of cities (e.g., src/main/resources/map.txt)
			{3} - number of monsters to create (e.g., 40000)

To run this project from command line use

	java -Dlog4j.configuration=file:{1} -jar {2} {3} {4}

	where 	{1} - path to log4j.properties (e.g., target/log4j.properties)
			{2} - path to jar (e.g., target/monsters-1.0-SNAPSHOT-jar-with-dependencies.jar)
			(2) - path to file with map of cities (e.g., target/map.txt)
			{3} - number of monsters to create (e.g., 40000)

=========================
Design decisions
=========================

Scalability

    Only vertical scaling is taking into account to not over-complicate code for this task. So parallel data processing
    is used to iterate through cities on the map. Two dimensional array to store map of cities is not used as it can
    have many empty elements and it is difficult to transform it to new smaller array after iteration completed and
    some cities were destroyed.

Synchronization

    Synchronization is used for the following operations: moving monsters and removing connections between cities.
    In both cases city and its neighbours are locked in appropriate order.
    Input and output data are processed sequentially for now.

Output to console

    To output to console slf4j logger is used as it provides more flexible configuration with different patterns for
    log file and std output. Also it ensures synchronization (Documentation for System.out.println method does not
    ensure that but most JVM implementations has synchronization).

=========================
Assumptions
=========================

Input data

    Data in UTF-8 format. City name does not contain '=' character. City name and directions are separated by single
    space.

Initialization

    Only one monster can be created in each city.

Iteration

    If 2 monsters are exchanged between 2 cities the cities are not destroyed and monsters are still alive.
    If 2 or more monsters in a city the city is destroyed and monsters are dead.

Output data

    Order of directions for not remaining cities are not taking into account.
